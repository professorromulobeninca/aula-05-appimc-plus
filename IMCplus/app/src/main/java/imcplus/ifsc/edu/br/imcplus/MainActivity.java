package imcplus.ifsc.edu.br.imcplus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {


 class Imc {


        public double calculaIMC (Double altura, double peso) {
            Double imc = peso /(altura * altura);
            DecimalFormat f = new DecimalFormat("#.####");
            return Double.parseDouble(f.format(imc));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void calculaIMC(View view) {
        Double peso= Double.parseDouble(((EditText) findViewById(R.id.editTextPeso)).getText().toString());
        Double altura= Double.parseDouble(((EditText) findViewById(R.id.editTextAltura)).getText().toString());
        TextView textViewResultado = (TextView) findViewById( R.id.textViewResultadoIMC);
        double imc =(new Imc() ).calculaIMC(altura,peso);
        textViewResultado.setText(Double.toString( imc));

        ImageView imgview = (ImageView) findViewById(R.id.imageView);
        if (imc<18.5) {
            imgview.setImageResource(R.drawable.abaixopeso);
        }else if (imc>=18.6 &&  imc<25){
            imgview.setImageResource(R.drawable.normal);
        }else if (imc>=25 &&  imc<30){
            imgview.setImageResource(R.drawable.sobrepeso);
        }else if (imc>=30 &&  imc<35){
            imgview.setImageResource(R.drawable.obesidade1);
        }else if (imc>35 &&  imc<=40){
            imgview.setImageResource(R.drawable.obesidade2);
        }else if (imc>35 &&  imc<=40){
            imgview.setImageResource(R.drawable.obesidade3);
        }
    }
}
